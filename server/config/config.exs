# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :depict,
  ecto_repos: [Depict.Repo]

# Configures the endpoint
config :depict, DepictWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "VSJ/R93cvBcXkuSMhX0DbklVA6D+q19cvO2ht1sg5eHXgRbj/sQ71gIHlQk2BxY1",
  render_errors: [view: DepictWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: Depict.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:user_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
