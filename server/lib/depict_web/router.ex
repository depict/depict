defmodule DepictWeb.Router do
  use DepictWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", DepictWeb do
    pipe_through :api
  end
end
